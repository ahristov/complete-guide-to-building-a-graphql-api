# Complete guide to building a GraphQL API

Contains notes and code from studying [Complete guide to building a GraphQL API](https://www.udemy.com/complete-guide-to-building-a-graphql-api/learn/v4/overview)

Framework:

- GraphQL

To run the app:

```bash
cd code
yarn
yarn start
```

## Section 1: What is Graph QL

### Section 1, Lecture 2: GraphQL characteristics

- Efficient
  - No overfetching or underfetching
  - Multiple queries - one request
  - Support for relational data
- Type system
  - Contract between client and server
  - Server-side validation
  - Meaningful errors
  - Additional safety
- Single endpoint
  - No "endpoint" hell
  - Evolve API without breaking
  - Facebook never versioned!
  - "Versionless" API

### Section 1, Lecture 3: Blog API

The API consist of 3 types of objects:

- Blog post
- Author
- Comment

## Section 2: Core concepts

Concepts:

- Fields and arguments
- Aliases and fragments
- Variables
- Queries and mutations

### Section 2, Lecture 5: Fields and Arguments

Example queries:

- Get the titles from all posts and who wrote the posts:

```js
query {
  posts {
    title
    author {
      name
    }
  }
}

...

{
  "data": {
    "posts": [
    {
      "title": "My first blog post",
      "author": {
        "name": "Xavier ..."
      }
    },
      ...
    ]
  }
}
```

**Operation** and **Field**:

    Operation: "query" - name of the operation. The operation has fields.

    Fields: Unit of data - "posts", "title".

- Get the title of a post by ID and all the comments to it deom 2017:

```js
query {
  post (id: 3) {
    title
    comments(year: 2017) {
      name
    }
  }
}
```

### Section 2, Lecture 6: Aliases and Fragments

Because the names of the fields in the query will match the result field names, we get _name conflicts on the output_, we can't have something like:

```js
query {
  post (id: 3) { title }
  post (id: 4) { title }
}
```

, because we can't output valid JSON.

**Aliases** are:

    Aliases: Allow to rename the result of a field.
    Ideal to query same type of objects more than one time.

Example of using aliases:

```js
query {
  firstPost: post (id: 3) { title }
  secondPost: post (id: 4) { title }
}
```

Now there is another problem - we get _very repetitive queries_, describing the same over and over.

**Fragments** are:

    Fragments: remove duplicated fields from queries,
    put inside reusable blocks.

Example:

```js

query {
  firstPost: post (id: 3) { ...basicPostDetails }
  secondPost: post (id: 4) { ...basicPostDetails }
}

fragment: basicPostDetails on Post {
  title,
  content,
  author {
    name
  }
}
```

### Section 2, Lecture 7: Variables

Instead of _hard-coding variables inside queries_, we can define **input variables**. We describe the _type_ can also say it is required by adding exclamation mark `!`:

```js
query($postId: Int!) {
  post (id: $postId) { ...basicPostDetails }
 ...
```

If variable is required, GraphQL will not execute if value not present.

Also, we need to define a JSON object and send it along with the query, so tat we can sent actual parameters:

![Example with variables](./doc/img/variables_example.png)

### Section 2, Lecture 8: Queries and Mutations

How do we _change data on the server_?

**Mutations** are:

    Mutation: accepts JSON object that describes what is to change

Example - we say what we want to change and we can say what we want to receive back:

```js
mutation {
  addPost(post: {
    title: "New blog post",
    content: "This is the content ...",
    author: "77e12ab"
  }) {
    title,
    author: { name }
  }
}
```

Putting data into queries is not a good practice, we should be using variables. In this case we are using special type `PostInput` defined on the GraphQL server:

```js
mutation($post: PostData!) {
  addPost(post: $post) {
    title,
    author: { name }
  }
}
```

![Example of a mutation](./doc/img/mutation_example.png)

What is the real difference between queries and mutations?

- We can always see of the operation will change some data or not
- Queries are executed in parallel, while mutations are executed one after the other
- This is to prevent race conditions as one mutation may need the data from previous mutation

## Section 3: Preparation to build app

### Section 3, Lecture 10: Directory structure

Create directory structure:

    /src
      /schema
        /mutations
          addComment.js
          addPost.js
        /queries
          author.js
          post.js
          posts.js
        /types
          Author.js
          Comment.js
          Post.js
        index.js (export the schema - all the files above)
      index.js (loading GraphQL library, loading the schema, executing queries)
    /dist

### Section 3, Lecture 11: Installing dependencies

Init project:

    $ npm init
    ...
    package name: (complete-guide-to-building-a-graphql-api) graphql-api-blog

Install dependencies:

    yarn add graphql
    yarn add --dev babel-cli babel-preset-env babel-plugin-transform-object-rest-spread

### Section 3, Lecture 12: Configure Babel

Create `.babelrc`:

```json
{
  "presets": ["env"],
  "plugins": ["transform-object-rest-spread"]
}
```

We install the default preset `env` and the `spread` plugin.

Create _build_ script in `package.json` that transpilers the code and runs it:

```json
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build": "babel src --out-dir dist/ && node ./dist/index.js"
  },
```

Then we can build the code:

  yarn run build


