class FakeDatabase {
  constructor() {
    this.authors = [
      { id: 'a001', name: 'Author One', email: 'author.one@mail.com' },
      { id: 'a002', name: 'Author Two', email: 'author.two@mail.com' },
      { id: 'a003', name: 'Author Three', email: 'author.three@mail.com' },
    ]

    this.blogPosts = [
      { id: 1, title: 'First blog post', content: 'This is my first blog post', author: 'a001' },
      { id: 2, title: 'Second blog post', content: 'This is my second blog post', author: 'a003' },
      { id: 3, title: 'Third blog post', content: 'This is my third blog post', author: 'a002' },
    ]

    this.comments = [
      { id: 1, postId: 1, name: 'Anonymous', content: 'Good luck with your first blog post!' },
      { id: 2, postId: 1, name: 'Nick', content: 'Great first blog post!' },
      { id: 3, postId: 3, name: 'John', content: 'Your third blog post shows a good progress!' },
    ]
  }

  //----------------------------------------------------------------------
  //      READ METHODS
  //----------------------------------------------------------------------
  getBlogPosts() {
    // Here you make a DB connection + query + return data
    return this.blogPosts
  }

  getBlogPost(id) {
    return this.blogPosts.filter((post) => {
      return post.id === id
    })[0]
  }

  getCommentsForPost(postId) {
    return ths.comments.filter((comment) => {
      return comment.postId === postId
    })
  }

  getAuthor(id) {
    return this.authors.filter((author) => {
      return author.id === id
    })
  }

  getPostsOfAuthor(authorId) {
    return this.getBlogPosts().filter((blogPost) => {
      return blogPost.author === authorId
    })
  }

  //----------------------------------------------------------------------
  //      READ METHODS
  //----------------------------------------------------------------------
  addNewBlogPost(post) {
    post.Id = this.blogPosts.length + 1
    this.blogPosts.push(post)
    return post
  }
}

export const fakeDatabase = new FakeDatabase()