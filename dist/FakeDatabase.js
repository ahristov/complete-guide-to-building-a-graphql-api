'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FakeDatabase = function () {
  function FakeDatabase() {
    _classCallCheck(this, FakeDatabase);

    this.authors = [{ id: 'a001', name: 'Author One', email: 'author.one@mail.com' }, { id: 'a002', name: 'Author Two', email: 'author.two@mail.com' }, { id: 'a003', name: 'Author Three', email: 'author.three@mail.com' }];

    this.blogPosts = [{ id: 1, title: 'First blog post', content: 'This is my first blog post', author: 'a001' }, { id: 2, title: 'Second blog post', content: 'This is my second blog post', author: 'a003' }, { id: 3, title: 'Third blog post', content: 'This is my third blog post', author: 'a002' }];

    this.comments = [{ id: 1, postId: 1, name: 'Anonymous', content: 'Good luck with your first blog post!' }, { id: 2, postId: 1, name: 'Nick', content: 'Great first blog post!' }, { id: 3, postId: 3, name: 'John', content: 'Your third blog post shows a good progress!' }];
  }

  //----------------------------------------------------------------------
  //      READ METHODS
  //----------------------------------------------------------------------


  _createClass(FakeDatabase, [{
    key: 'getBlogPosts',
    value: function getBlogPosts() {
      // Here you make a DB connection + query + return data
      return this.blogPosts;
    }
  }, {
    key: 'getBlogPost',
    value: function getBlogPost(id) {
      return this.blogPosts.filter(function (post) {
        return post.id === id;
      })[0];
    }
  }, {
    key: 'getCommentsForPost',
    value: function getCommentsForPost(postId) {
      return ths.comments.filter(function (comment) {
        return comment.postId === postId;
      });
    }
  }, {
    key: 'getAuthor',
    value: function getAuthor(id) {
      return this.authors.filter(function (author) {
        return author.id === id;
      });
    }
  }, {
    key: 'getPostsOfAuthor',
    value: function getPostsOfAuthor(authorId) {
      return this.getBlogPosts().filter(function (blogPost) {
        return blogPost.author === authorId;
      });
    }

    //----------------------------------------------------------------------
    //      READ METHODS
    //----------------------------------------------------------------------

  }, {
    key: 'addNewBlogPost',
    value: function addNewBlogPost(post) {
      post.Id = this.blogPosts.length + 1;
      this.blogPosts.push(post);
      return post;
    }
  }]);

  return FakeDatabase;
}();

var fakeDatabase = exports.fakeDatabase = new FakeDatabase();